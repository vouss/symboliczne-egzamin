import math

print(math.sqrt(25))

li = [1,2,3,4,5,6]

print([element**0.5 for element in li])

params = {"server":"mpilgrim", "database":"master", "uid":"sa", "pwd":"secret"}

#print(params.keys())
#print(params.values())
new_list = ["%s <-> %s" % (k+'KEY', v+'ADD') for k, v in params.items()]
print(new_list)

new_list2 = [(item.upper(), char + '[ ' + item + ' ]') for item in ['abc','bcde','cde','def','efgh'] for char in item if char == 'c' ]
print(new_list2)
tuples = [(x, x**2) for x in [0,1,2,3,4,5,6,7,8,9]]
print(tuples)
